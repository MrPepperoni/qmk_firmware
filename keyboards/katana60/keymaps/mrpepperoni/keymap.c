/* Copyright 2017 Baris Tosun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "katana60.h"
#include QMK_KEYBOARD_H
#include "action_layer.h"
#include "eeconfig.h"


#define _______ KC_TRNS
#define XXXXXXX KC_NO
#define FN_TAB LT(_FUNCTION,KC_TAB)
#define CT_ESC MT(MOD_LCTL,KC_ESC)
#define FN_BSPC LT(1, KC_BSPC)



#define _QWERTY 0 // Default
#define _LOWER 3 // Numbers
#define _RAISE 4 // Symbols
#define _FUNCTION 15 // Text Editing
#define _ADJUST 16

enum custom_keycodes {
    QWERTY = SAFE_RANGE,
    LOWER,
    RAISE,
    ADJUST
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_QWERTY] = LAYOUT( /* Base */
  KC_GRV,  KC_BSLS, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    XXXXXXX, KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    XXXXXXX, KC_DEL,
  FN_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    XXXXXXX,          XXXXXXX, KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
  CT_ESC,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    XXXXXXX,          XXXXXXX, KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_ENT,
  KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    XXXXXXX, XXXXXXX, XXXXXXX, KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,
  KC_LCTL, KC_LGUI, KC_LALT, LOWER,                     KC_SPACE,XXXXXXX, KC_SPACE,         RAISE,   KC_RALT, KC_RCTL, KC_GRV,  KC_QUOT
    ),
[_LOWER] = LAYOUT(
  _______, _______, _______, _______, _______, _______, _______, DF(0),   KC_PSLS, KC_PAST, KC_PMNS, _______, _______, _______, _______,
  KC_ESC,  KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, _______,          _______, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, _______,
  _______, KC_F1,    KC_F2,   KC_F3,   KC_F4,   KC_F5,  _______,          _______, KC_F6,   KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, _______,
  _______, KC_F7,    KC_F8,   KC_F9,   KC_F10,  KC_F11, _______, _______, _______, KC_F12,  _______, _______, KC_MUTE, _______, KC_PIPE,
  _______, _______, _______, _______,                   KC_BSPC, TO(_FUNCTION), KC_BSPC,          KC_PDOT, KC_PENT, _______, _______, _______
    ),
[_RAISE] = LAYOUT(
  KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  XXXXXXX, _______,
  KC_ESC,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    _______,          _______, KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    _______,
  _______, KC_4,    KC_5,    KC_6,    KC_PLUS, KC_F5,   _______,          _______, KC_F6,   KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, _______,
  KC_ENT,  KC_7,    KC_8,    KC_9,    KC_MINS, KC_F11,  _______, _______, _______, KC_F12,  KC_NUHS, KC_NUBS, KC_MUTE, _______, KC_BSLS,
  _______, KC_COMM, KC_0,    KC_DOT,                    KC_BSPC, TO(_FUNCTION), KC_BSPC,          _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY
    ),
[_ADJUST] = LAYOUT(
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______, _______,          _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______, _______,          _______, _______, _______, _______, _______, _______, _______,
  KC_CAPS, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______,                   _______, TO(_FUNCTION), _______,          _______, _______, _______, _______, RESET
    ),
[_FUNCTION] = LAYOUT(
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______, _______,          _______, _______, KC_PGUP, KC_UP,   KC_PGDN, _______, KC_BSPC,
  _______, _______, _______, _______, _______, _______, _______,          _______, KC_HOME, KC_LEFT, KC_DOWN, KC_RIGHT,KC_END,  KC_ENT,
  KC_CAPS, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______,                   KC_SPACE, TO(_QWERTY), KC_SPACE,          _______, _______, _______, _______, RESET
    )


};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

void led_set_user(uint8_t usb_led) {

}

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        persistent_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
  }
  return true;
}

