NKRO_ENABLE = yes            # Nkey Rollover - if this doesn't work, see here: https://github.com/tmk/tmk_keyboard/wiki/FAQ#nkro-doesnt-work
BOOTMAGIC_ENABLE = yes
RGBLIGHT_ENABLE = yes
TAP_DANCE_ENABLE = yes

